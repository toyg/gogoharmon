#!/usr/bin/env bash
GOPATH=$PWD:$GOPATH
TAG=`git describe --tags | tail -n1`
echo Building release $TAG ...
FINALDIR=roboharmon_$TAG
DISTDIR=dist
OUTDIR=$DISTDIR/$FINALDIR
echo Cleaning $DISTDIR ...
rm -r $DISTDIR
mkdir -p $OUTDIR
echo Building ...
go build -o $OUTDIR/roboharmon || exit
cp LICENSE $OUTDIR/
cp README.md $OUTDIR/
cd $DISTDIR || exit
echo Zip it all up ...
zip -r ${FINALDIR}_MACOS.zip $FINALDIR
cd .. || exit
echo Done
