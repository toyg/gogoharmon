@echo off
FOR /F "tokens=* USEBACKQ" %%F IN (`git describe --tags`) DO (
    SET VERSION=%%F
)
set VERDIR=roboharmon_%VERSION%
set DISTDIR=dist
set OUTDIR=%DISTDIR%\%VERDIR%

set Z7="C:\Program Files\7-Zip\7z.exe"

go build -o bin\roboharmon.exe
rd /s %DISTDIR%
mkdir %OUTDIR%
copy LICENSE %OUTDIR%\
copy README.md %OUTDIR%\
copy bin\roboharmon.exe %OUTDIR%\
cd dist
%Z7% a -r %VERDIR%_WINDOWS.zip %VERDIR%
cd ..

:exit