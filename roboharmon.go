package main

import (
	"github.com/docopt/docopt-go"
	"gitlab.com/toyg/gogoharmon"
	"log"
	"os"
	"path/filepath"
	"time"
)

var __version__ = "RoboHarmon 1.1.2 (go)"

var Usage = `Usage:
     roboharmon [--audio | --video | --all] [--user <username>] [--clearpass] [--from <from_date>] [--to <to_date>] [--outdir <output_dir>] [-h | --help]

Options:
    --audio     Audio only (the full podcast)
    --video     Video only (requires Harmontown active subscription).
                This is the default.
    --all       Both audio and video (requires Harmontown active subscription).
                This is done sequentially (all videos first).
    --user <username>       Harmontown username for login. If not provided,
                            you will be challenged for it.
    --clearpass          Delete previously-stored credentials
    --from <from_date>      Start downloading episodes published after this date.
                            Date format is YYYY-MM-DD.
                            E.g. to download episodes from 2017 onwards:
                            roborharmon --from 2017-01-01
    --to <from_date>     Only download episodes published before this date.
                         Date format is YYYY-MM-DD.
                         E.g. to download episodes up to 2016 included:
                         roborharmon --to 2016-12-31
    --outdir <output_dir>   Directory that will contain the saved files.
                            If not specified, defaults to the current directory.
    -h, --help   Print this message
`

func main() {
	var conf struct {
		Audio     bool
		Video     bool
		All       bool
		Clearpass bool
		From      string
		To        string
		User      string
		Outdir    string
	}
	var pwd string
	parser := &docopt.Parser{OptionsFirst: true}
	args, _ := parser.ParseArgs(Usage, os.Args[1:], __version__)
	err := args.Bind(&conf)
	if err != nil {
		log.Fatal("Failure parsing arguments", err)
	}

	modeMsg := "VIDEO"
	if conf.All {
		conf.Audio = true
		conf.Video = true
		modeMsg = "ALL"
	} else {
		if conf.Audio {
			conf.Video = false
			modeMsg = "AUDIO"
		} else {
			conf.Video = true
		}
	}
	modeMsg = "Mode: " + modeMsg
	log.Println(modeMsg)
	if conf.Clearpass {
		_ = gogoharmon.ClearSecrets()
	}
	if conf.User == "" && (conf.All || conf.Video) {
		conf.User = gogoharmon.GetUsername()
	}
	if conf.Clearpass {
		_ = gogoharmon.ClearPassword(conf.User)
	}
	if conf.All || conf.Video {
		pwd = gogoharmon.GetPassword(conf.User)
	}

	fromDate := time.Date(2012, 1, 1, 0, 0, 0, 1, time.UTC)
	toDate := time.Date(2019, 12, 31, 0, 0, 0, 1, time.UTC)
	var parseErr error
	if conf.From != "" {
		log.Println("From: " + conf.From)
		fromDate, parseErr = gogoharmon.ParseDate(conf.From)
		if parseErr != nil {
			log.Fatal("Invalid format passed to --from, must be YYYY-MM-DD")
		}
	}
	if conf.To != "" {
		log.Println("To: " + conf.To)
		toDate, parseErr = gogoharmon.ParseDate(conf.To)
		if parseErr != nil {
			log.Fatal("Invalid format passed to --to, must be YYYY-MM-DD")
		}
	}

	conf.Outdir, _ = filepath.Abs(conf.Outdir)

	if _, err := os.Stat(conf.Outdir); os.IsNotExist(err) {
		err = os.MkdirAll(conf.Outdir, 0777)
		if err != nil {
			log.Fatal("could not create folder "+conf.Outdir, err)
		}
	}
	log.Println("Output dir: " + conf.Outdir)
	dl := gogoharmon.NewDownloader(conf.Outdir)
	if conf.All || conf.Audio {
		log.Println("Downloading audio files...")
		dl.DownloadAudio(fromDate, toDate)
	}

	if conf.All || conf.Video {
		dl.LogOn(conf.User, pwd)
		dl.DownloadVideo(fromDate, toDate, 0)
	}
	log.Println(`
Job done.
        
        "Nice shootin', son. What's your name?"
        "Murphy."
`)
}
