package gogoharmon

var ROBOHARMON_SEC_LABEL = "Roboharmon secret"
var ROBOHARMON_ACCESS_GROUP = "com.autoepm.roboharmon"

func SetKeyringPassword(service string, user string, secret string) error {
	var err error
	item := keychain.NewGenericPassword(service, user, ROBOHARMON_SEC_LABEL,
		[]byte(secret), ROBOHARMON_ACCESS_GROUP)
	item.SetSynchronizable(keychain.SynchronizableNo)
	item.SetAccessible(keychain.AccessibleWhenUnlocked)
	err = keychain.AddItem(item)
	if err == keychain.ErrorDuplicateItem {
		oldItem := keychain.NewItem()
		oldItem.SetSecClass(keychain.SecClassGenericPassword)
		oldItem.SetService(service)
		oldItem.SetAccount(user)
		oldItem.SetAccessGroup(ROBOHARMON_ACCESS_GROUP)
		oldItem.SetMatchLimit(keychain.MatchLimitOne)
		oldItem.SetReturnAttributes(true)
		oldItem.SetReturnData(true)
		err = keychain.UpdateItem(oldItem, item)
	}
	return err
}

func GetKeyringPassword(service string, user string) (string, error) {
	dataBytes, err := keychain.GetGenericPassword(service, user, ROBOHARMON_SEC_LABEL, ROBOHARMON_ACCESS_GROUP)
	return string(dataBytes), err
}

func DeleteKeyringPassword(service string, user string) error {
	err := keychain.DeleteGenericPasswordItem(service, user)
	return err
}
