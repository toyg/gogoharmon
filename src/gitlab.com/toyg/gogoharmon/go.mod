module gitlab.com/toyg/gogoharmon

go 1.13

require (
	github.com/PuerkitoBio/goquery v1.5.0 // indirect
	github.com/cheggaaa/pb/v3 v3.0.3
	github.com/danieljoos/wincred v1.0.2
	github.com/gosuri/uilive v0.0.3 // indirect
	github.com/gosuri/uiprogress v0.0.1
	github.com/keybase/go-keychain v0.0.0-20191114153608-ccd67945d59e
	github.com/mmcdole/gofeed v1.0.0-beta2
	github.com/mmcdole/goxpp v0.0.0-20181012175147-0068e33feabf // indirect
	github.com/stretchr/testify v1.4.0 // indirect
	golang.org/x/crypto v0.0.0-20190308221718-c2843e01d9a2
	golang.org/x/net v0.0.0-20191209160850-c0dbc17a3553
)
