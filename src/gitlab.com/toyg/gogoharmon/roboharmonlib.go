package gogoharmon

import (
	"bufio"
	"encoding/json"
	"fmt"
	"github.com/cheggaaa/pb/v3"
	"github.com/gosuri/uiprogress"
	"github.com/mmcdole/gofeed"
	"golang.org/x/crypto/ssh/terminal"
	"golang.org/x/net/html"
	html2 "html"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"os"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"syscall"
	"time"
)

const loginUrl = "https://www.harmontown.com/wp-login.php"
const wpJsonURL = "https://www.harmontown.com/wp-json/wp/v2/posts/"
const feedUrl = "http://feeds.feedburner.com/HarmontownPodcast"

const keyringService = "roboharmon"
const keyringKeyUser = "__roboharmon_default_user__"

var titleRegex = regexp.MustCompile(`[:<>\\\.\*\|"'’?` + "`]")

var loginHeaders = map[string]string{
	"User-Agent": "RoboHarmon 2.0 - Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:71.0) Gecko/20100101 Firefox/71.0",
	"Referer":    loginUrl,
	"Cookie":     "wordpress_test_cookie=WP+Cookie+check",
}

type VideoPost struct {
	title          string
	postLink       string
	videoLink      string
	outputFilename string
}

func NewVideoPost(jsonPost map[string]interface{}) VideoPost {
	title := jsonPost["title"].(map[string]interface{})
	saneTitle := sanitizeTitle(title["rendered"].(string))
	postDateGMT, _ := time.Parse("2006-01-02T15:04:05", jsonPost["date_gmt"].(string))
	targetName := postDateGMT.Format("2006-01-02 ") + saneTitle + ".mp4"
	post := VideoPost{
		title:          saneTitle,
		videoLink:      "",
		outputFilename: targetName,
		postLink:       jsonPost["link"].(string),
	}
	return post
}

type Downloader struct {
	outputDir       string
	inventoryPath   string
	client          *http.Client
	loggedOn        bool
	downloadedFiles []string
	wg              sync.WaitGroup
}

func NewDownloader(dlDir string) Downloader {
	jar, _ := cookiejar.New(nil)
	var wg sync.WaitGroup
	dl := Downloader{
		outputDir:       dlDir,
		inventoryPath:   filepath.Join(dlDir, ".gogoharmon_inventory.json"),
		downloadedFiles: make([]string, 0),
		client: &http.Client{
			Jar: jar,
		},
		loggedOn: false,
		wg:       wg,
	}
	dl.loadInventory()
	uiprogress.Start()
	return dl
}

func (downloader *Downloader) LogOn(user string, pwd string) {
	data := url.Values{
		"log":         {user},
		"pwd":         {pwd},
		"wp-submit":   {"Log In"},
		"redirect-to": {""},
		"testcookie":  {"1"},
		"rememberme":  {"forever"},
	}
	// first call to get cookies
	reqGet, _ := http.NewRequest("GET", loginUrl, nil)
	for k, v := range loginHeaders {
		reqGet.Header.Set(k, v)
	}
	_, _ = downloader.client.Do(reqGet)

	reqPost, _ := http.NewRequest("POST", loginUrl, strings.NewReader(data.Encode()))
	reqPost.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	for k, v := range loginHeaders {
		reqPost.Header.Set(k, v)
	}
	resp, err := downloader.client.Do(reqPost)
	if err != nil {
		log.Fatal("Could not log on. ", err)
	}
	bodyBytes, err := ioutil.ReadAll(resp.Body)
	body := string(bodyBytes)
	for _, ck := range downloader.client.Jar.Cookies(resp.Request.URL) {
		if strings.Contains(ck.Name, "wordpress_logged_in") {
			downloader.loggedOn = true
			break
		}
	}
	defer resp.Body.Close()
	if !downloader.loggedOn {
		errorRegex, _ := regexp.Compile(`<div id="login_error">(?P<error>.*)<br />`)
		match := errorRegex.FindStringSubmatch(body)
		log.Fatal("Failure logging on: " + strings.Trim(match[1], "\t\n "))
	}
}

func (downloader *Downloader) loadInventory() {
	if _, err := os.Stat(downloader.inventoryPath); err == nil {
		f2, err := os.Open(downloader.inventoryPath)
		dec := json.NewDecoder(f2)
		err = dec.Decode(&downloader.downloadedFiles)
		if err != nil {
			log.Fatal("Failed loading inventory_path", err)
		}
		f2.Close()
	}
}

func (downloader *Downloader) saveInventory() {
	f1, err := os.Create(downloader.inventoryPath)
	if err == nil {
		enc := json.NewEncoder(f1)
		_ = enc.Encode(downloader.downloadedFiles)
		f1.Close()
	} else {
		log.Fatal("Could not write inventory_path to "+downloader.inventoryPath, err)
	}

}

func (downloader *Downloader) saveMedia(link string, targetName string) (bool, error) {
	for idx := 0; idx < len(downloader.downloadedFiles); idx++ {
		if downloader.downloadedFiles[idx] == link {
			return true, nil
		}
	}
	log.Println("Downloading " + targetName + "...")
	targetPath := filepath.Join(downloader.outputDir, targetName)
	tempPath := filepath.Join(os.TempDir(), targetName)
	out, err := os.Create(tempPath)
	if err != nil {
		log.Println("Failure creating temporary file to " + tempPath)
		return false, err
	}
	// Create the file, but give it a tmp file extension, this means we won't overwrite a
	// file until it's downloaded, but we'll remove the tmp extension once downloaded.
	defer out.Close()
	// download
	resp, err := downloader.client.Get(link)

	if err != nil {
		return false, err
	}
	bar := pb.Full.Start64(resp.ContentLength)
	bar.Set(pb.Bytes, true)
	bar.SetRefreshRate(time.Second)
	barReader := bar.NewProxyReader(resp.Body)
	_, err = io.Copy(out, barReader)
	out.Close() // without this, Windows fails the rename step
	resp.Body.Close()
	bar.Finish()
	if err == nil {
		err = MoveFile(tempPath, targetPath)
		if err == nil {
			downloader.downloadedFiles = append(downloader.downloadedFiles, link)
			downloader.saveInventory()
			return true, nil
		} else {
			log.Println("ERROR ", err)
		}
	}
	// still here? smt went wrong
	return false, err
}

func (downloader *Downloader) DownloadAudio(fromDate time.Time, toDate time.Time) {
	fp := gofeed.NewParser()
	feed, _ := fp.ParseURL(feedUrl)
	for i := 0; i < len(feed.Items); i++ {
		item := *feed.Items[i]
		if IsAcceptableDate(*item.PublishedParsed, fromDate, toDate) {

			target_path := item.PublishedParsed.Format("2006-01-02") + " " +
				sanitizeTitle(item.Title) + ".mp3"
			_, err := downloader.saveMedia(item.Enclosures[0].URL, target_path)
			if err != nil {
				log.Println("ERROR: ", err)
			}
		}
	}
}

func (downloader *Downloader) findVideoLink(post *VideoPost) string {
	response, _ := downloader.client.Get(post.postLink)
	bodyBytes, _ := ioutil.ReadAll(response.Body)
	body := string(bodyBytes)
	links := make([]string, 0)
	z := html.NewTokenizer(strings.NewReader(body))
	for keepLooping := true; keepLooping; {
		tt := z.Next()
		switch {
		case tt == html.ErrorToken:
			keepLooping = false
			break
		case tt == html.StartTagToken:
			t := z.Token()
			isAnchor := t.Data == "a"
			if isAnchor {
				isDownload := false
				for _, a := range t.Attr {
					if a.Key == "title" {
						if a.Val == "Download" {
							isDownload = true
						}
					}
				}
				if isDownload {
					for _, a := range t.Attr {
						if a.Key == "href" {
							if strings.HasSuffix(a.Val, ".mp4") {
								links = append(links, a.Val)
							}
						}
					}
				}
			}
		}
	}
	if len(links) == 0 {
		return ""
	}
	if len(links) == 1 {
		post.videoLink = links[0]
		return links[0]
	} else {
		for _, link := range links {
			if strings.HasSuffix(link, "-final.mp4") {
				post.videoLink = link
				return link
			}
		}
	}
	// still here? weird. Well, pick the first
	print("weird multiple versions:\n" + strings.Join(links, "\n"))
	post.videoLink = links[0]
	return links[0]
}

func (downloader *Downloader) DownloadVideo(fromDate time.Time, toDate time.Time, offset int) {
	if !downloader.loggedOn {
		log.Fatal("You need to be logged on to download viedos")
	}
	pageSize := 100
	u, _ := url.Parse(wpJsonURL)
	q := u.Query()
	q.Add("categories", "8,70")
	q.Add("orderby", "date")
	q.Add("order", "asc")
	q.Add("per_page", strconv.Itoa(pageSize))
	q.Add("before", toDate.Format(time.RFC3339))
	q.Add("after", fromDate.Format(time.RFC3339))
	q.Add("offset", strconv.Itoa(offset))
	u.RawQuery = q.Encode()

	res, _ := http.Get(u.String())
	result, _ := ioutil.ReadAll(res.Body)
	defer res.Body.Close()
	jsonResult := []map[string]interface{}{}
	json.Unmarshal([]byte(result), &jsonResult)
	for _, post := range jsonResult {
		vp := NewVideoPost(post)
		downloader.findVideoLink(&vp)
		downloader.saveMedia(vp.videoLink, vp.outputFilename)
	}
	hack_date := time.Date(2019, 10, 15, 0, 0, 0, 1, time.UTC)
	if (fromDate.Before(hack_date) && toDate.After(hack_date)) ||
		IsEqualDate(hack_date, fromDate) ||
		IsEqualDate(hack_date, toDate) {
		hack_post := VideoPost{
			title:     "Nice Dunking, Cool Swishing",
			postLink:  "https://www.harmontown.com/2019/10/video-episode-353-nice-dunking-cool-swishing/",
			videoLink: "",
			outputFilename: hack_date.Format("2006-01-02 ") +
				sanitizeTitle("Nice Dunking, Cool Swishing") + ".mp4",
		}
		downloader.findVideoLink(&hack_post)
		downloader.saveMedia(hack_post.videoLink, hack_post.outputFilename)
	}

	if len(jsonResult) == pageSize {
		downloader.DownloadVideo(fromDate, toDate, offset+pageSize)
	}
	defer res.Body.Close()
}

func sanitizeTitle(text string) string {
	unescaped := html2.UnescapeString(text)
	return titleRegex.ReplaceAllLiteralString(unescaped, "")
}

func prompt(message string) string {
	fmt.Println(message)
	reader := bufio.NewReader(os.Stdin)
	result, _ := reader.ReadString('\n')
	result = strings.Trim(result, "\n")
	return result
}

func GetUsername() string {

	user, err := GetKeyringPassword(keyringService, keyringKeyUser)
	if err != nil || strings.Trim(user, " \n") == "" {
		user = prompt("Enter your Harmontown username: ")
		if user == "" {
			errorMsg := "Empty username, cannot proceed"
			err = DeleteKeyringPassword(keyringService, keyringKeyUser)
			log.Fatal(errorMsg, err)
		}
		err = SetKeyringPassword(keyringService, keyringKeyUser, strings.Trim(user, "\n"))
		if err != nil {
			log.Println("ERROR saving user: ", err)
		}
	}
	return user
}

func GetPassword(user string) string {
	pwd, err := GetKeyringPassword(keyringService, user)
	if err != nil || strings.Trim(pwd, " \n") == "" {
		fmt.Print("Enter your Harmontwon password: ")
		bytePassword, err := terminal.ReadPassword(int(syscall.Stdin))
		fmt.Print("\n")
		if err != nil {
			errorMsg := "Error entering password, cannot proceed"
			log.Fatal(errorMsg)
		}
		pwd = string(bytePassword)
		err = SetKeyringPassword(keyringService, user, pwd)
		if err != nil {
			log.Println("ERROR saving password: ", err)
		}
	}
	return pwd
}

func ClearSecrets() error {
	return DeleteKeyringPassword(keyringService, keyringKeyUser)
}

func ClearPassword(user string) error {
	return DeleteKeyringPassword(keyringService, user)
}

func ParseDate(dateStr string) (time.Time, error) {
	layout := "2006-01-02"
	return time.Parse(layout, dateStr)
}

func IsAcceptableDate(dateObj time.Time, fromD time.Time, toD time.Time) bool {
	if IsEqualDate(dateObj, fromD) || IsEqualDate(dateObj, toD) {
		return true
	}
	diffFrom := dateObj.Sub(fromD)
	diffTo := toD.Sub(dateObj)
	return diffFrom.Seconds() > 0 && diffTo.Seconds() > 0
}

func IsEqualDate(date1 time.Time, date2 time.Time) bool {
	// I can't believe how silly this is. Go does not have an "equal" for pure dates.
	// So we hack together one here.
	d1, _ := strconv.ParseInt(date1.Format("20060102"), 10, 32)
	d2, _ := strconv.ParseInt(date2.Format("20060102"), 10, 32)
	return d1 == d2
}

func MoveFile(source, destination string) (err error) {
	src, err := os.Open(source)
	if err != nil {
		return err
	}
	defer src.Close()
	fi, err := src.Stat()
	if err != nil {
		return err
	}
	flag := os.O_WRONLY | os.O_CREATE | os.O_TRUNC
	perm := fi.Mode() & os.ModePerm
	dst, err := os.OpenFile(destination, flag, perm)
	if err != nil {
		return err
	}
	defer dst.Close()
	_, err = io.Copy(dst, src)
	if err != nil {
		dst.Close()
		os.Remove(destination)
		return err
	}
	err = dst.Close()
	if err != nil {
		return err
	}
	err = src.Close()
	if err != nil {
		return err
	}
	err = os.Remove(source)
	if err != nil {
		return err
	}
	return nil
}
