package gogoharmon

import (
	"fmt"
	"github.com/danieljoos/wincred"
)

type UserNotMatchingError struct {
	userPassed string
	userFound  string
}

func (e *UserNotMatchingError) Error() string {
	return fmt.Sprintf("Requested %s but found %s", e.userPassed, e.userFound)
}

func makeCredReference(service string, user string) string {
	return service + "::" + user
}

func SetKeyringPassword(service string, user string, secret string) error {
	cred := wincred.NewGenericCredential(makeCredReference(service, user))
	cred.UserName = user
	cred.CredentialBlob = []byte(secret)
	err := cred.Write()
	return err
}
func GetKeyringPassword(service string, user string) (string, error) {
	cred, err := wincred.GetGenericCredential(makeCredReference(service, user))
	if err != nil {
		return "", err
	}
	if cred.UserName != user {
		return "", &UserNotMatchingError{
			userPassed: user,
			userFound:  cred.UserName,
		}
	}
	return string(cred.CredentialBlob), err

}

func DeleteKeyringPassword(service string, user string) error {
	cred, err := wincred.GetGenericCredential(makeCredReference(service, user))
	if err != nil {
		return err
	}
	if cred.UserName != user {
		return nil
	}
	return cred.Delete()
}
