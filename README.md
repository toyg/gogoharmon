RoboHarmon
==========

The ultimate Harmontown mass-downloader. 
No double-downloads, date filter, audio and/or video, safe password handling, flexible output dir, 
and absolutely no third-act problems whatsoever.

Install
-------

Mac / Windows:  Download the appropriate release at https://gitlab.com/toyg/gogoharmon/-/releases

For Linux: use https://gitlab.com/toyg/roboharmon instead, or build this from source.

Unzip the release, open a command prompt, and `cd` to the folder where Roboharmon.exe is  - e.g.:
```cmd
cd /d c:\Users\your_user\Desktop\roboharmon
```

Usage
-----
By default, RoboHarmon will try to download all videos available.
You can also specify to download the audio version only, or both.

RoboHarmon will not download the same file twice, unless it was not fully downloaded. 
You can rename files at any point, they will not be redownloaded.

You can reduce the date range and specify an output folder. 
Download progress will be saved in the output folder itself, so you can move it around 
and restart your downloads without any issue.

Downloading videos requires a subscription to Harmontown (it's cheapy-peepy!). 
Your password will be safely stored in your operating-system secure facility.

To stop roboharmon at any point, use Ctrl-C.

Command syntax: 
```
roboharmon [--audio | --video | --all] [--user <username>] [--from <from_date>]
           [--to <to_date>] [--outdir <output_dir>] [-h | --help]

Options:
    --audio     Audio only (the podcast)
    --video     Video only (requires Harmontown active subscription).
                This is the default.
    --all       Both audio and video (requires Harmontown active subscription).
                This is done sequentially (all videos first).
    --user <username>       Harmontown username for login. If not provided,
                            you will be challenged for it.
    --clearpass      Delete previously-saved password and prompt again for it.
    --from <from_date>      Start downloading episodes published after this date.
                            Date format is YYYY-MM-DD.
                            E.g. to download episodes from 2017 onwards:
                            roborharmon --from 2017-01-01
    --to <from_date>        Only download episodes published before this date.
                            Date format is YYYY-MM-DD.
                            E.g. to download episodes up to 2016 included:
                            roborharmon --to 2016-12-31
    --outdir <output_dir>   Directory that will contain the saved files.
                            If not specified, defaults to the current directory.
    -h, --help              Print this message
```

Examples:
```cmd

# get it all (you need to have a paid subscription)
roboharmon --all

# get only podcasts published in 2018
roboharmon --audio --from 2017-12-31 --to 2019-01-01

# get all videos published after 1 May 2019 (you need a subscription)
roboharmon --from 2019-05-01 --outdir c:\Users\your_user\Desktop
```

Credits
=======
Roboharmon @ 2019 Giacomo Lacava. 
Released under the terms of the BSD Licence (i.e. just keep the damn credits when you clone/rip).

Inspired by https://github.com/holden-nelson/harmonscript - I started tweaking that but ended up rewriting it all in Go.